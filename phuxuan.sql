-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 30, 2019 lúc 09:01 AM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `phuxuan`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `advertisements`
--

CREATE TABLE `advertisements` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_information` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `banners`
--

INSERT INTO `banners` (`id`, `name`, `image`, `id_information`, `created_at`, `updated_at`) VALUES
(1, 'phu xuan hostel', 'banners\\October2019\\H99sgRjPWA9qQEK4CiJp.jpg', 2, '2019-10-23 03:28:15', '2019-10-23 03:28:15');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `booking_rooms`
--

CREATE TABLE `booking_rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quality` int(11) NOT NULL,
  `style_room` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_arrival` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_department` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `booking_rooms`
--

INSERT INTO `booking_rooms` (`id`, `name`, `quality`, `style_room`, `phone`, `email`, `date_of_arrival`, `date_of_department`, `created_at`, `updated_at`) VALUES
(1, 'long', 2, 'Phòng đôi', '4543535', 'manhlong99@gmail.com', '10/23/2019', '10/23/2019', '2019-10-23 02:58:27', '2019-10-23 02:58:27');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(2, NULL, 1, 'Category 2', 'category-2', '2019-10-17 19:08:31', '2019-10-17 19:08:31');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 7, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(57, 7, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(58, 7, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(59, 7, 'phone', 'text', 'Phone', 1, 1, 1, 1, 1, 1, '{}', 4),
(60, 7, 'address', 'text', 'Address', 1, 1, 1, 1, 1, 1, '{}', 5),
(61, 7, 'facebook', 'text', 'Facebook', 1, 1, 1, 1, 1, 1, '{}', 6),
(62, 7, 'content', 'text', 'Excerpt', 0, 0, 1, 1, 1, 1, '{}', 7),
(63, 7, 'contents', 'rich_text_box', 'Description', 1, 0, 1, 1, 1, 1, '{}', 8),
(64, 7, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1920\",\"height\":null},\"quality\":\"80%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 10),
(65, 7, 'slug', 'text', 'Slug', 0, 0, 1, 1, 1, 1, '{}', 11),
(66, 7, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, '{}', 12),
(67, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(68, 8, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(69, 8, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
(70, 8, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1920\",\"height\":null},\"quality\":\"80%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 4),
(71, 8, 'id_information', 'select_dropdown', 'Id Information', 1, 0, 1, 1, 1, 1, '{\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(72, 8, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 5),
(73, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(74, 9, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(75, 9, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(76, 9, 'price', 'text', 'Price', 1, 1, 1, 1, 1, 1, '{}', 3),
(77, 9, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1920\",\"height\":null},\"quality\":\"80%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 5),
(79, 9, 'amount', 'number', 'Amount', 1, 0, 1, 1, 1, 1, '{}', 7),
(80, 9, 'size', 'text', 'Size', 1, 0, 1, 1, 1, 1, '{}', 8),
(81, 9, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{}', 9),
(82, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(83, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(84, 10, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(85, 10, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 3),
(86, 10, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1920\",\"height\":null},\"quality\":\"80%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 4),
(87, 10, 'id_style_room', 'select_dropdown', 'Id Style Room', 1, 0, 1, 1, 1, 1, '{\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(88, 10, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 5),
(89, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(90, 11, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(91, 11, 'name', 'text', 'Name', 1, 1, 1, 0, 0, 1, '{}', 2),
(92, 11, 'quality', 'text', 'Quality', 1, 1, 1, 0, 0, 1, '{}', 4),
(93, 11, 'style_room', 'text', 'Style Room', 1, 1, 1, 0, 0, 1, '{}', 5),
(94, 11, 'phone', 'text', 'Phone', 1, 1, 1, 0, 0, 1, '{}', 3),
(95, 11, 'email', 'text', 'Email', 1, 1, 1, 0, 0, 1, '{}', 6),
(96, 11, 'date_of_arrival', 'text', 'Date Of Arrival', 1, 0, 1, 0, 0, 1, '{}', 7),
(97, 11, 'date_of_department', 'text', 'Date Of Department', 1, 0, 1, 0, 0, 1, '{}', 8),
(98, 11, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 9),
(99, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(100, 12, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(101, 12, 'name', 'text', 'Name', 1, 1, 1, 0, 0, 1, '{}', 2),
(102, 12, 'email', 'text', 'Email', 1, 1, 1, 0, 0, 1, '{}', 3),
(103, 12, 'subject', 'text', 'Subject', 1, 1, 1, 1, 1, 1, '{}', 4),
(104, 12, 'content', 'text', 'Content', 1, 0, 1, 0, 0, 1, '{}', 5),
(105, 12, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, '{}', 6),
(106, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(107, 13, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(108, 13, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(109, 13, 'icon', 'text', 'Icon', 0, 1, 1, 1, 1, 1, '{}', 3),
(110, 13, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 5),
(111, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(112, 14, 'id', 'text', 'Id', 1, 1, 0, 0, 0, 0, '{}', 1),
(113, 14, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 2),
(114, 14, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 3),
(115, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(116, 7, 'logo', 'image', 'Logo', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1920\",\"height\":null},\"quality\":\"80%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 9),
(117, 13, 'status', 'select_dropdown', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 4),
(118, 9, 'contents', 'rich_text_box', 'Contents', 1, 1, 1, 1, 1, 1, '{}', 6),
(119, 9, 'promotion_price', 'text', 'Promotion Price', 0, 1, 1, 1, 1, 1, '{}', 4),
(120, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(121, 17, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(122, 17, 'price_day', 'number', 'Price Day', 1, 1, 1, 1, 1, 1, '{}', 3),
(123, 17, 'price_week', 'number', 'Price Week', 1, 1, 1, 1, 1, 1, '{}', 4),
(124, 17, 'price_month', 'number', 'Price Month', 1, 1, 1, 1, 1, 1, '{}', 5),
(125, 17, 'content', 'rich_text_box', 'Content', 0, 1, 1, 1, 1, 1, '{}', 6),
(126, 17, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 1, 0, 1, '{}', 7),
(127, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(7, 'information', 'information', 'Information', 'Information', 'voyager-home', 'App\\Information', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-10-17 19:20:20', '2019-10-17 21:13:09'),
(8, 'banners', 'banners', 'Banner', 'Banners', 'voyager-images', 'App\\Banner', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-10-17 19:23:24', '2019-10-17 19:23:24'),
(9, 'style_rooms', 'style-rooms', 'Style Room', 'Style Rooms', NULL, 'App\\StyleRoom', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-10-17 19:35:16', '2019-10-18 01:02:03'),
(10, 'image_rooms', 'image-rooms', 'Image Room', 'Image Rooms', NULL, 'App\\ImageRoom', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-10-17 19:37:05', '2019-10-17 19:37:05'),
(11, 'booking_rooms', 'booking-rooms', 'Booking Room', 'Booking Rooms', NULL, 'App\\BookingRoom', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-10-17 20:11:40', '2019-10-17 20:11:40'),
(12, 'feedback', 'feedback', 'Feedback', 'Feedback', 'voyager-bubble', 'App\\Feedback', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-10-17 20:13:28', '2019-10-17 20:13:28'),
(13, 'services', 'services', 'Service', 'Services', 'voyager-wallet', 'App\\Service', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-10-17 20:14:26', '2019-10-18 00:23:40'),
(14, 'subscribers', 'subscribers', 'Subscriber', 'Subscribers', 'voyager-mail', 'App\\Subscriber', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-10-17 20:15:49', '2019-10-17 20:15:49'),
(15, 'servicess', 'servicess', 'Servicess', 'Servicess', NULL, 'App\\Servicess', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-10-18 01:45:16', '2019-10-18 01:45:16'),
(16, 'service_other', 'service-other', 'Service Other', 'Service Others', NULL, 'App\\ServiceOther', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-10-18 01:50:34', '2019-10-18 01:50:34'),
(17, 'service_others', 'service-others', 'Service Other', 'Service Others', NULL, 'App\\ServiceOther', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-10-18 01:53:02', '2019-10-18 02:05:24');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `feedback`
--

CREATE TABLE `feedback` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `image_rooms`
--

CREATE TABLE `image_rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_style_room` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `image_rooms`
--

INSERT INTO `image_rooms` (`id`, `name`, `image`, `id_style_room`, `created_at`, `updated_at`) VALUES
(1, 'Phòng đôi 1', 'image-rooms\\October2019\\Jf1pNUsVSzFLw2KCqyRv.jpg', 1, '2019-10-23 02:43:00', '2019-10-23 02:49:53'),
(2, 'phòng đôi 2', 'image-rooms\\October2019\\BKlHywKFRnJ0pn7pNLxj.jpg', 1, '2019-10-23 02:44:00', '2019-10-23 02:49:12'),
(3, 'phòng đôi 3', 'image-rooms\\October2019\\Ty5OHmjMJomW7henmuxo.jpg', 1, '2019-10-23 02:45:03', '2019-10-23 02:45:03'),
(4, 'Phòng đơn 1', 'image-rooms\\October2019\\lOjICtYDjl2YST5B5sRn.jpg', 3, '2019-10-23 02:45:34', '2019-10-23 02:45:34'),
(5, 'phong dơn 2', 'image-rooms\\October2019\\7h5koNDsItTtrZKMmEUP.jpg', 3, '2019-10-23 02:46:01', '2019-10-23 02:46:01'),
(6, 'phong don 3', 'image-rooms\\October2019\\iBmpRKAKNUzw7Y9DzDbL.jpg', 3, '2019-10-23 02:46:31', '2019-10-23 02:46:31'),
(7, 'phong don 4', 'image-rooms\\October2019\\VNa92zlo2E9m5fgPqF6W.jpg', 4, '2019-10-23 02:47:12', '2019-10-23 02:47:12'),
(8, 'phong don 5', 'image-rooms\\October2019\\m54odG4nAFCGvgSAVVrx.jpg', 4, '2019-10-23 02:48:00', '2019-10-23 02:48:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `information`
--

CREATE TABLE `information` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `logo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `information`
--

INSERT INTO `information` (`id`, `name`, `email`, `phone`, `address`, `facebook`, `content`, `contents`, `image`, `slug`, `created_at`, `updated_at`, `logo`) VALUES
(2, 'Phu Xuan Hostel', 'nga2991@gmail.com', '02343835869', '11/116 Nguyễn Lộ Trạch – TP Huế', 'https://www.facebook.com/cybrandteam', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '<p>Ph&uacute; Xu&acirc;n Hostel - điểm nghỉ dưỡng ph&ugrave; hợp cho bạn. Nằm ở vị tr&iacute; gần trung t&acirc;m th&agrave;nh phố, từ nh&agrave; nghỉ Ph&uacute; Xu&acirc;n bạn c&oacute; thể dễ d&agrave;ng di chuyển đến c&aacute;c trung t&acirc;m giải tr&iacute; lớn như VinCom, Big C hay c&aacute;c địa danh du lịch như Đại Nội, ch&ugrave;a Thi&ecirc;n Mụ, &hellip; Đặc biệt Nh&agrave; nghỉ cung cấp đầy đủ c&aacute;c dịch vụ du lịch phục vụ mọi nhu cầu của Qu&yacute; kh&aacute;ch như: cung cấp c&aacute;c tour, du thuyền, ca Huế tr&ecirc;n s&ocirc;ng Hương, ca m&uacute;a cung đ&igrave;nh, đặt chỗ c&aacute;c chuyến bay quốc nội, t&agrave;u hỏa, xe du lịch&hellip; Đến với Ph&uacute; Xu&acirc;n Hostel, chắc chắn sẽ l&agrave;m qu&yacute; kh&aacute;ch h&agrave;i l&ograve;ng</p>', 'information\\October2019\\S8QXm8wWqgJCNr00nM1d.jpg', 'phu-xuan-hostel', '2019-10-17 21:20:12', '2019-10-23 02:20:17', 'information\\October2019\\Hu1bk7rhqBoDEUhtfZHA.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-10-17 19:08:31', '2019-10-17 19:08:31');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-10-17 19:08:31', '2019-10-17 19:08:31', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 12, '2019-10-17 19:08:31', '2019-10-18 01:53:35', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 2, '2019-10-17 19:08:31', '2019-10-17 20:43:49', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 3, '2019-10-17 19:08:31', '2019-10-17 20:43:53', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 16, '2019-10-17 19:08:31', '2019-10-18 01:53:35', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2019-10-17 19:08:31', '2019-10-17 20:42:58', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2019-10-17 19:08:31', '2019-10-17 20:42:58', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2019-10-17 19:08:31', '2019-10-17 20:42:58', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2019-10-17 19:08:31', '2019-10-17 20:42:58', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 17, '2019-10-17 19:08:31', '2019-10-18 01:53:35', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 15, '2019-10-17 19:08:31', '2019-10-18 01:53:35', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 13, '2019-10-17 19:08:31', '2019-10-18 01:53:35', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 14, '2019-10-17 19:08:32', '2019-10-18 01:53:35', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2019-10-17 19:08:32', '2019-10-17 20:42:58', 'voyager.hooks', NULL),
(15, 1, 'Information', '', '_self', 'voyager-info-circled', '#000000', NULL, 4, '2019-10-17 19:20:20', '2019-10-17 20:47:18', 'voyager.information.index', 'null'),
(16, 1, 'Banners', '', '_self', 'voyager-images', NULL, 23, 2, '2019-10-17 19:23:24', '2019-10-17 20:43:20', 'voyager.banners.index', NULL),
(17, 1, 'Style Rooms', '', '_self', 'voyager-home', '#000000', NULL, 6, '2019-10-17 19:35:16', '2019-10-17 20:47:05', 'voyager.style-rooms.index', 'null'),
(18, 1, 'Image Rooms', '', '_self', 'voyager-images', '#000000', 23, 1, '2019-10-17 19:37:05', '2019-10-17 20:45:38', 'voyager.image-rooms.index', 'null'),
(19, 1, 'Booking Rooms', '', '_self', 'voyager-calendar', '#000000', NULL, 5, '2019-10-17 20:11:41', '2019-10-17 20:46:29', 'voyager.booking-rooms.index', 'null'),
(20, 1, 'Feedback', '', '_self', 'voyager-bubble', NULL, NULL, 11, '2019-10-17 20:13:28', '2019-10-18 01:53:35', 'voyager.feedback.index', NULL),
(21, 1, 'Services', '', '_self', 'voyager-wallet', NULL, NULL, 7, '2019-10-17 20:14:26', '2019-10-17 20:44:11', 'voyager.services.index', NULL),
(22, 1, 'Subscribers', '', '_self', 'voyager-mail', NULL, NULL, 10, '2019-10-17 20:15:49', '2019-10-18 01:53:35', 'voyager.subscribers.index', NULL),
(23, 1, 'Images', '/admin/image', '_self', 'voyager-folder', '#000000', NULL, 9, '2019-10-17 20:42:46', '2019-10-18 01:53:35', NULL, ''),
(26, 1, 'Service Others', '', '_self', NULL, NULL, NULL, 8, '2019-10-18 01:53:02', '2019-10-18 01:53:35', 'voyager.service-others.index', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(21, '2017_08_05_000000_add_group_to_settings_table', 1),
(22, '2017_11_26_013050_add_user_role_relationship', 1),
(23, '2017_11_26_015000_create_user_roles_table', 1),
(24, '2018_03_11_000000_add_user_settings', 1),
(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
(26, '2018_03_16_000000_make_settings_value_nullable', 1),
(27, '2019_10_06_041157_create_style_rooms_table', 1),
(28, '2019_10_07_084718_create_services_table', 1),
(29, '2019_10_07_090700_create_tours_table', 1),
(30, '2019_10_07_091609_create_advertisements_table', 1),
(31, '2019_10_07_092943_create_information_table', 1),
(32, '2019_10_08_031729_create_banners_table', 1),
(33, '2019_10_09_041450_create_image_rooms_table', 1),
(34, '2019_10_10_022140_create_connects_table', 1),
(35, '2019_10_15_034005_create_booking_rooms_table', 1),
(36, '2019_10_16_020308_create_subscribers_table', 1),
(37, '2019_10_17_022648_create_feedback_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2019-10-17 19:08:32', '2019-10-17 19:08:32');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(2, 'browse_bread', NULL, '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(3, 'browse_database', NULL, '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(4, 'browse_media', NULL, '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(5, 'browse_compass', NULL, '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(6, 'browse_menus', 'menus', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(7, 'read_menus', 'menus', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(8, 'edit_menus', 'menus', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(9, 'add_menus', 'menus', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(10, 'delete_menus', 'menus', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(11, 'browse_roles', 'roles', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(12, 'read_roles', 'roles', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(13, 'edit_roles', 'roles', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(14, 'add_roles', 'roles', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(15, 'delete_roles', 'roles', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(16, 'browse_users', 'users', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(17, 'read_users', 'users', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(18, 'edit_users', 'users', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(19, 'add_users', 'users', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(20, 'delete_users', 'users', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(21, 'browse_settings', 'settings', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(22, 'read_settings', 'settings', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(23, 'edit_settings', 'settings', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(24, 'add_settings', 'settings', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(25, 'delete_settings', 'settings', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(26, 'browse_categories', 'categories', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(27, 'read_categories', 'categories', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(28, 'edit_categories', 'categories', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(29, 'add_categories', 'categories', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(30, 'delete_categories', 'categories', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(31, 'browse_posts', 'posts', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(32, 'read_posts', 'posts', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(33, 'edit_posts', 'posts', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(34, 'add_posts', 'posts', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(35, 'delete_posts', 'posts', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(36, 'browse_pages', 'pages', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(37, 'read_pages', 'pages', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(38, 'edit_pages', 'pages', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(39, 'add_pages', 'pages', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(40, 'delete_pages', 'pages', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(41, 'browse_hooks', NULL, '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(42, 'browse_information', 'information', '2019-10-17 19:20:20', '2019-10-17 19:20:20'),
(43, 'read_information', 'information', '2019-10-17 19:20:20', '2019-10-17 19:20:20'),
(44, 'edit_information', 'information', '2019-10-17 19:20:20', '2019-10-17 19:20:20'),
(45, 'add_information', 'information', '2019-10-17 19:20:20', '2019-10-17 19:20:20'),
(46, 'delete_information', 'information', '2019-10-17 19:20:20', '2019-10-17 19:20:20'),
(47, 'browse_banners', 'banners', '2019-10-17 19:23:24', '2019-10-17 19:23:24'),
(48, 'read_banners', 'banners', '2019-10-17 19:23:24', '2019-10-17 19:23:24'),
(49, 'edit_banners', 'banners', '2019-10-17 19:23:24', '2019-10-17 19:23:24'),
(50, 'add_banners', 'banners', '2019-10-17 19:23:24', '2019-10-17 19:23:24'),
(51, 'delete_banners', 'banners', '2019-10-17 19:23:24', '2019-10-17 19:23:24'),
(52, 'browse_style_rooms', 'style_rooms', '2019-10-17 19:35:16', '2019-10-17 19:35:16'),
(53, 'read_style_rooms', 'style_rooms', '2019-10-17 19:35:16', '2019-10-17 19:35:16'),
(54, 'edit_style_rooms', 'style_rooms', '2019-10-17 19:35:16', '2019-10-17 19:35:16'),
(55, 'add_style_rooms', 'style_rooms', '2019-10-17 19:35:16', '2019-10-17 19:35:16'),
(56, 'delete_style_rooms', 'style_rooms', '2019-10-17 19:35:16', '2019-10-17 19:35:16'),
(57, 'browse_image_rooms', 'image_rooms', '2019-10-17 19:37:05', '2019-10-17 19:37:05'),
(58, 'read_image_rooms', 'image_rooms', '2019-10-17 19:37:05', '2019-10-17 19:37:05'),
(59, 'edit_image_rooms', 'image_rooms', '2019-10-17 19:37:05', '2019-10-17 19:37:05'),
(60, 'add_image_rooms', 'image_rooms', '2019-10-17 19:37:05', '2019-10-17 19:37:05'),
(61, 'delete_image_rooms', 'image_rooms', '2019-10-17 19:37:05', '2019-10-17 19:37:05'),
(62, 'browse_booking_rooms', 'booking_rooms', '2019-10-17 20:11:41', '2019-10-17 20:11:41'),
(63, 'read_booking_rooms', 'booking_rooms', '2019-10-17 20:11:41', '2019-10-17 20:11:41'),
(64, 'edit_booking_rooms', 'booking_rooms', '2019-10-17 20:11:41', '2019-10-17 20:11:41'),
(65, 'add_booking_rooms', 'booking_rooms', '2019-10-17 20:11:41', '2019-10-17 20:11:41'),
(66, 'delete_booking_rooms', 'booking_rooms', '2019-10-17 20:11:41', '2019-10-17 20:11:41'),
(67, 'browse_feedback', 'feedback', '2019-10-17 20:13:28', '2019-10-17 20:13:28'),
(68, 'read_feedback', 'feedback', '2019-10-17 20:13:28', '2019-10-17 20:13:28'),
(69, 'edit_feedback', 'feedback', '2019-10-17 20:13:28', '2019-10-17 20:13:28'),
(70, 'add_feedback', 'feedback', '2019-10-17 20:13:28', '2019-10-17 20:13:28'),
(71, 'delete_feedback', 'feedback', '2019-10-17 20:13:28', '2019-10-17 20:13:28'),
(72, 'browse_services', 'services', '2019-10-17 20:14:26', '2019-10-17 20:14:26'),
(73, 'read_services', 'services', '2019-10-17 20:14:26', '2019-10-17 20:14:26'),
(74, 'edit_services', 'services', '2019-10-17 20:14:26', '2019-10-17 20:14:26'),
(75, 'add_services', 'services', '2019-10-17 20:14:26', '2019-10-17 20:14:26'),
(76, 'delete_services', 'services', '2019-10-17 20:14:26', '2019-10-17 20:14:26'),
(77, 'browse_subscribers', 'subscribers', '2019-10-17 20:15:49', '2019-10-17 20:15:49'),
(78, 'read_subscribers', 'subscribers', '2019-10-17 20:15:49', '2019-10-17 20:15:49'),
(79, 'edit_subscribers', 'subscribers', '2019-10-17 20:15:49', '2019-10-17 20:15:49'),
(80, 'add_subscribers', 'subscribers', '2019-10-17 20:15:49', '2019-10-17 20:15:49'),
(81, 'delete_subscribers', 'subscribers', '2019-10-17 20:15:49', '2019-10-17 20:15:49'),
(82, 'browse_servicess', 'servicess', '2019-10-18 01:45:16', '2019-10-18 01:45:16'),
(83, 'read_servicess', 'servicess', '2019-10-18 01:45:16', '2019-10-18 01:45:16'),
(84, 'edit_servicess', 'servicess', '2019-10-18 01:45:16', '2019-10-18 01:45:16'),
(85, 'add_servicess', 'servicess', '2019-10-18 01:45:16', '2019-10-18 01:45:16'),
(86, 'delete_servicess', 'servicess', '2019-10-18 01:45:16', '2019-10-18 01:45:16'),
(87, 'browse_service_other', 'service_other', '2019-10-18 01:50:34', '2019-10-18 01:50:34'),
(88, 'read_service_other', 'service_other', '2019-10-18 01:50:34', '2019-10-18 01:50:34'),
(89, 'edit_service_other', 'service_other', '2019-10-18 01:50:34', '2019-10-18 01:50:34'),
(90, 'add_service_other', 'service_other', '2019-10-18 01:50:34', '2019-10-18 01:50:34'),
(91, 'delete_service_other', 'service_other', '2019-10-18 01:50:34', '2019-10-18 01:50:34'),
(92, 'browse_service_others', 'service_others', '2019-10-18 01:53:02', '2019-10-18 01:53:02'),
(93, 'read_service_others', 'service_others', '2019-10-18 01:53:02', '2019-10-18 01:53:02'),
(94, 'edit_service_others', 'service_others', '2019-10-18 01:53:02', '2019-10-18 01:53:02'),
(95, 'add_service_others', 'service_others', '2019-10-18 01:53:02', '2019-10-18 01:53:02'),
(96, 'delete_service_others', 'service_others', '2019-10-18 01:53:02', '2019-10-18 01:53:02');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(66, 1),
(67, 1),
(68, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'DRAFT', 0, '2019-10-17 19:08:32', '2019-10-18 00:36:38'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\r\n                <h2>We can use all kinds of format!</h2>\r\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-10-17 19:08:32', '2019-10-17 19:08:32');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-10-17 19:08:31', '2019-10-17 19:08:31'),
(2, 'user', 'Normal User', '2019-10-17 19:08:31', '2019-10-17 19:08:31');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `services`
--

INSERT INTO `services` (`id`, `name`, `icon`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Vòi sen', NULL, '2019-10-17 21:26:56', '2019-10-17 21:26:56', NULL),
(2, 'TV', NULL, '2019-10-17 21:27:14', '2019-10-17 21:27:14', NULL),
(3, 'Đồ vệ sinh cá nhân miễn phí', NULL, '2019-10-17 21:27:22', '2019-10-17 21:27:22', NULL),
(4, 'Điều hòa không khí', NULL, '2019-10-17 21:27:30', '2019-10-17 21:27:30', NULL),
(5, 'Ban công', NULL, '2019-10-17 21:27:45', '2019-10-17 21:27:45', NULL),
(6, 'Quạt máy', NULL, '2019-10-17 21:27:56', '2019-10-17 21:27:56', NULL),
(7, 'Nhà vệ sinh', NULL, '2019-10-17 21:28:02', '2019-10-17 21:28:02', NULL),
(8, 'Phòng tắm riêng', NULL, '2019-10-17 21:28:10', '2019-10-17 21:28:10', NULL),
(9, 'Dép', NULL, '2019-10-17 21:28:18', '2019-10-17 21:28:18', NULL),
(10, 'Truyền hình cáp', NULL, '2019-10-17 21:28:36', '2019-10-17 21:28:36', NULL),
(11, 'Sàn lát gạch/đá cẩm thạch', NULL, '2019-10-17 21:28:54', '2019-10-17 21:28:54', NULL),
(12, 'Tủ để quần áo', NULL, '2019-10-17 21:29:02', '2019-10-17 21:29:02', NULL),
(13, 'Khăn tắm', NULL, '2019-10-17 21:29:10', '2019-10-17 21:29:10', NULL),
(14, 'Bộ khăn trải giường', NULL, '2019-10-17 21:29:17', '2019-10-17 21:29:17', NULL),
(15, 'Bàn ăn (dùng chung)', NULL, '2019-10-17 21:29:28', '2019-10-17 21:29:28', NULL),
(16, 'Giấy vệ sinh', NULL, '2019-10-17 21:29:39', '2019-10-17 21:29:39', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `service_others`
--

CREATE TABLE `service_others` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_day` int(11) NOT NULL,
  `price_week` int(11) NOT NULL,
  `price_month` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `service_others`
--

INSERT INTO `service_others` (`id`, `name`, `price_day`, `price_week`, `price_month`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Phương tiện đi lại', 100000, 0, 0, '<p>Xe m&aacute;y tay ga: 150.000 ng&agrave;y</p>\r\n<p>Xem m&aacute;y số: 100.000 ng&agrave;y</p>', '2019-10-18 01:55:00', '2019-10-18 02:05:40');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'S', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\October2019\\N07hJL0KRQ8nVs6JeS5Z.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'PHU XUAN HOSTEL', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Phu Xuan Hostel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `style_rooms`
--

CREATE TABLE `style_rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `promotion_price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `style_rooms`
--

INSERT INTO `style_rooms` (`id`, `name`, `price`, `image`, `contents`, `amount`, `size`, `slug`, `created_at`, `updated_at`, `promotion_price`) VALUES
(1, 'Phòng đôi', 300000, 'style-rooms\\October2019\\Vt9PjkQoFRa0O6GegVLZ.jpg', '<p style=\"text-align: justify;\">Ph&ograve;ng đ&ocirc;i c&oacute; 2 ph&ograve;ng rộng 22m<sup>2&nbsp;</sup>&nbsp;,mỗi ph&ograve;ng c&oacute; sức chứa 2 người, c&oacute; đầy đủ trang bị gồm: giường, tủ &aacute;o quần, ti vi c&aacute;p...</p>', '3', '20 m2', 'phong-doi', '2019-10-17 20:51:00', '2019-10-23 02:41:14', NULL),
(3, 'Phòng đơn ( có cửa sổ )', 240000, 'style-rooms\\October2019\\SmvPT1VxPdFjvpE0AI4Z.jpg', '<p>Ph&ograve;ng đơn c&oacute; cửa số c&oacute; 2 ph&ograve;ng rộng 18m<sup>2&nbsp;</sup>&nbsp;,mỗi ph&ograve;ng c&oacute; sức chứa 2 người, c&oacute; đầy đủ trang bị gồm: giường, tủ &aacute;o quần, ti vi c&aacute;p...</p>', '2', '20 m2', 'phong-doi-lau-1', '2019-10-17 20:55:00', '2019-10-23 02:39:15', NULL),
(4, 'Phòng Đơn (Không có cửa sổ)', 200000, 'style-rooms\\October2019\\n9cYfvTZpNUXNNsG5HDQ.jpg', '<p>Ph&ograve;ng đơn kh&ocirc;ng c&oacute; cửa số c&oacute; 6 ph&ograve;ng rộng 18m<sup>2&nbsp;</sup> ,mỗi ph&ograve;ng c&oacute; sức chứa 2 người, c&oacute; đầy đủ trang bị gồm: giường, tủ &aacute;o quần, ti vi c&aacute;p...</p>', '2', '20 m2', 'phong-don-khong-co-cua-so', '2019-10-17 20:57:00', '2019-10-23 02:36:20', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tours`
--

CREATE TABLE `tours` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2019-10-17 19:08:32', '2019-10-17 19:08:32'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2019-10-17 19:08:32', '2019-10-17 19:08:32');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$.O65QgZBhA61EZ6Whl7lRuoeGlXrpziM9EbyTX2T2BkRDAdQSeGr6', 'dU4ADNayQ0JOroPGh7DoQiXJEOKuBmY7aVqhzCl1vYAMDYncuLyY4vm52glX', NULL, '2019-10-17 19:08:31', '2019-10-17 19:08:31');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banners_id_information_foreign` (`id_information`);

--
-- Chỉ mục cho bảng `booking_rooms`
--
ALTER TABLE `booking_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Chỉ mục cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Chỉ mục cho bảng `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `image_rooms`
--
ALTER TABLE `image_rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `image_rooms_id_style_room_foreign` (`id_style_room`);

--
-- Chỉ mục cho bảng `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Chỉ mục cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Chỉ mục cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Chỉ mục cho bảng `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `service_others`
--
ALTER TABLE `service_others`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Chỉ mục cho bảng `style_rooms`
--
ALTER TABLE `style_rooms`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tours`
--
ALTER TABLE `tours`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Chỉ mục cho bảng `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `booking_rooms`
--
ALTER TABLE `booking_rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT cho bảng `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `image_rooms`
--
ALTER TABLE `image_rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `information`
--
ALTER TABLE `information`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT cho bảng `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `service_others`
--
ALTER TABLE `service_others`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `style_rooms`
--
ALTER TABLE `style_rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tours`
--
ALTER TABLE `tours`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `banners`
--
ALTER TABLE `banners`
  ADD CONSTRAINT `banners_id_information_foreign` FOREIGN KEY (`id_information`) REFERENCES `information` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `image_rooms`
--
ALTER TABLE `image_rooms`
  ADD CONSTRAINT `image_rooms_id_style_room_foreign` FOREIGN KEY (`id_style_room`) REFERENCES `style_rooms` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Các ràng buộc cho bảng `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
