<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("banners", function (Blueprint $table) {
            $table->foreign('id_information')->references('id')->on('information')->onDelete('cascade');
        });
        Schema::table("image_rooms", function (Blueprint $table) {
            $table->foreign('id_style_room')->references('id')->on('style_rooms')->onDelete('cascade');
        });
        // Schema::table("services", function (Blueprint $table) {
        //     $table->foreign('id_service_room')->references('id')->on('style_rooms')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connects');
    }
}
