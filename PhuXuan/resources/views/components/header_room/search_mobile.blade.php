<div id="search_mobile">
    <a href="#" class="side_panel"><i class="icon_close"></i></a>
    <div class="custom-search-input-2">
        <div class="form-group">
            <input class="form-control" type="text" placeholder="What are you looking..">
            <i class="icon_search"></i>
        </div>
        <div class="form-group">
            <input class="form-control" type="text" placeholder="Where">
            <i class="icon_pin_alt"></i>
        </div>
        <select class="wide">
            <option>All Categories</option>	
            <option>Shops</option>
            <option>Hotels</option>
            <option>Restaurants</option>
            <option>Bars</option>
            <option>Events</option>
            <option>Fitness</option>
        </select>
        <input type="submit" value="Search">
    </div>
</div>