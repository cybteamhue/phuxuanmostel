<div class="col-lg-9 col-12 top_menu ">
    <ul id="top_menu">
        <li><a href="{{ route('room')}}" class="btn_add">BOOK NOW</a></li>       
    </ul>
    <!-- /top_menu -->
    <a href="#menu" class="btn_mobile">
        <div class="hamburger hamburger--spin" id="hamburger">
            <div class="hamburger-box">
                <div class="hamburger-inner"></div>
            </div>
        </div>
    </a>
    
    <nav id="menu" class="main-menu">
        <ul>
            <li><span><a href="{{ route('home') }}"><i class="pe-7s-home"></i> TRANG CHỦ </a></span></li>
            <li><span><a href="{{ route('room')}}"><i class=" icon-bell"></i>  ĐẶT PHÒNG </a></span></li>     
            <li><span><a href="{{ route('blog') }}"><i class=" icon-newspaper"></i> TIN TỨC</a></span></li>
            <li><span><a href="#lienhe"><i class=" icon-contacts"></i>  LIÊN HỆ </a></span></li>
        </ul>
    </nav>
</div>
