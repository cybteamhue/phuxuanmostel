<div class="col-lg-9">
        <div class="row">
            @foreach($posts as $post)
            <div class="col-md-6">
                <article class="blog">
                    <figure>
                    <a href="{{ route('detail.Blog',$post->slug) }}"><img src="{{ Voyager::image( method_exists($post, 'thumbnail') ? $post->thumbnail('cropped') : $post->image ) }}" alt="{{$post->title}}">
                            <div class="preview"><span>Xem chi tiết</span></div>
                        </a>
                    </figure>
                    <div class="post_info">
                        <small>{{$post->created_at->format('d-m-Y')}}</small>
                        <h2><a href="{{ route('detail.Blog',$post->slug) }}">{{$post->title}}</a></h2>
                        <p class="expert">{!! $post->excerpt !!}.</p>
                    </div>
                </article>
                <!-- /article -->
            </div>
            @endforeach
            <!-- /col -->
        </div>
        <!-- /row -->

        <div class="pagination__wrapper add_bottom_30">
            <ul class="pagination">
                    {{ $posts->links()}}
            </ul>
        </div>
        <!-- /pagination -->
        
</div>