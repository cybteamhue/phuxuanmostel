<section class="hero_single version_4">
    <div class="wrapper">
          
                @foreach ($information->banner as $banner)
                <img src="{{ Voyager::image( method_exists($banner, 'thumbnail') ? $styleRoom->thumbnail('cropped') : $banner->image ) }}"  class="img-fluid" alt="{{$banner->name}}">
                @endforeach      
    
    </div>
</section>