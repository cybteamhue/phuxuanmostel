<div class="container-fluid margin_80_55">
    <div class="container main_title_2">
        <span><em></em></span>
        <h2>CÁC LOẠI PHÒNG</h2>
    </div>
    <div class="container">     
        <div class="row">
            @foreach($styleRooms as $styleRoom)
            <div class="col-md-4">
                <div class="strip grid">
                    <figure>
                        <a  class="wish_bt"></a>
                            <a href="{{ route('detail.Room',$styleRoom->slug) }}"><img src="{{ Voyager::image( method_exists($styleRoom, 'thumbnail') ? $styleRoom->thumbnail('cropped') : $styleRoom->image ) }}" class="img-fluid" alt="{{$styleRoom->name}}">
                            <div class="read_more"><span>Xem chi tiết</span></div>
                        </a>
                    
                    </figure>
                    <div class="wrapper">
                        <div class="wrapperstyle">
                            <h3 class="huewrapper"><a href="{{ route('detail.Room',$styleRoom->slug) }}">{{ $styleRoom->name }}</a></h3>
                            <p>{!! $styleRoom->contents !!}</p>
                        </div>
                        <h4>{{number_format($styleRoom->price, 0, ',', ',')}} <small>VNĐ/NGÀY</small></h4>
                    </div>

                    <ul>
                        <li><span class="loc_open">  <a href="{{ route('detail.Room',$styleRoom->slug) }}">Xem chi tiết</a></span></li>
                        <li class="detailroom">
                            <div class="score"><span>PHU XUAN<em>HOSTEL</em></span><strong></strong></div>
                        </li>
                    </ul>
                </div>
            </div>
            @endforeach
            <!-- /strip grid -->            
        </div>
    </div>
</div>