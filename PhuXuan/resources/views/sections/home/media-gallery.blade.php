<div class="container margin_60_35">
        <div class="main_title_2">
            <span><em></em></span>
            <h2>THƯ VIỆN ẢNH</h2>
            <p>Một số ảnh về phòng của nhà nghỉ</p>
        </div>
        <div class="grid-gallery">
            <ul class="magnific-gallery">
                 @foreach($styleRooms as $styleRoom)
                    @foreach($styleRoom->imageRoom as $imageRoom)
                    <li>
                        <figure>
                            <img src="{{ Voyager::image( method_exists($imageRoom, 'thumbnail') ? $imageRoom->thumbnail('cropped') : $imageRoom->image ) }}" style="width:100%; height:100%" alt="{{$imageRoom->name}}">
                            <figcaption>
                                <div class="caption-content">
                                    <a href="{{ Voyager::image( method_exists($imageRoom, 'thumbnail') ? $imageRoom->thumbnail('cropped') : $imageRoom->image ) }}"  data-effect="mfp-zoom-in">
                                        <i class="pe-7s-albums"></i>
                                        <p>PHÓNG TO ẢNH</p>
                                    </a>
                                </div>
                            </figcaption>
                        </figure>
                    </li>  
                    @endforeach  
                @endforeach              
            </ul>
        </div>
        <!-- /grid gallery -->
    </div>