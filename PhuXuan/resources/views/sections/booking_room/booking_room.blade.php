<div class="box_detail booking">
    <div class="price">
      <h5 class="reserrom">ĐẶT PHÒNG</h5>
      @if(session('message'))
        <div class="alert alert-success booking">
            <strong>{{session('message')}}</strong>
        </div>
      @endif 
    </div>
    <form action="{{ route('booking') }}" method="POST">
        @csrf
        <div class="form-group" id="input-dates">
          <input class="form-control pull-right" type="text" name="date_of_arrival" placeholder="Ngày đến" value>
          <i class="icon_calendar"></i>
          @if($errors->has('date_of_arrival'))
            <div class="error-warning">
                <span style="color:red">{{ $errors->first('date_of_arrival')}}</span>
            </div>
          @endif
        </div>
        <div class="form-group" id="input-datess">
          <input class="form-control pull-right" type="text" name="date_of_department" placeholder="Ngày đi" value>
          <i class="icon_calendar"></i>
          @if($errors->has('date_of_department'))
            <div class="error-warning">
                <span style="color:red">{{ $errors->first('date_of_department')}}</span>
            </div>
          @endif
        </div>     
        <div class="form-group clearfix">
            <div class="custom-select-form">
              <select class="wide" name="style_room">
                <option disabled selected >Loại phòng</option>
                @foreach ($styleRooms as $styleRoom)
                <option {{$styleRoom->id}}>{{$styleRoom->name}}</option>	
                @endforeach
              </select>
            </div>
          </div>
        <div class="form-group" >
          <input class="form-control" type="number" name="quality" placeholder="Số lượng phòng" required>
          <i class="pe-7s-compass"></i>
          @if($errors->has('quality'))
            <div class="error-warning">
                <span style="color:red">{{ $errors->first('quality')}}</span>
            </div>
          @endif
        </div>
        <div class="form-group" >
          <input class="form-control" type="text" name="name" placeholder="Họ tên" required>
          <i class="pe-7s-diskette"></i>
          @if($errors->has('name'))
            <div class="error-warning">
                <span style="color:red">{{ $errors->first('name')}}</span>
            </div>
          @endif
        </div>
        <div class="form-group" >           
          <input class="form-control" type="number" name="phone" placeholder="Số điện thoại" required>
          <i class="pe-7s-phone"></i>
          @if($errors->has('phone'))
            <div class="error-warning">
                <span style="color:red">{{ $errors->first('phone')}}</span>
            </div>
          @endif
        </div>
        <div class="form-group">            
          <input class="form-control" type="email" name="email" placeholder="Email" required>
          <i class="pe-7s-mail"></i>
          @if($errors->has('email'))
            <div class="error-warning">
                <span style="color:red">{{ $errors->first('email')}}</span>
            </div>
          @endif
        </div>
        <button type="submit" class=" add_top_30 btn_1 full-width purchase">ĐẶT NGAY</button>
    </form>     

  </div>
