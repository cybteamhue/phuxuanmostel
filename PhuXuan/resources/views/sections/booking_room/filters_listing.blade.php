<div class="filters_listing version_2 ">
    <div class="container">
        <ul class="clearfix">          
            <li>
                <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Ẩn bản đồ" data-text-original="Xem bản đồ">Xem bản đồ</a>
            </li>           
        </ul>
    </div>
    <!-- /container -->
</div>
<div class="collapse" id="collapseMap">
    <div id="map" class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3826.0960720423336!2d107.60507061434159!3d16.470672332930384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a11af0f3d891%3A0x812ac18221479b39!2zMSwgMTE2IE5ndXnhu4VuIEzhu5kgVHLhuqFjaCwgWHXDom4gUGjDuiwgVGjDoG5oIHBo4buRIEh14bq_LCBUaOG7q2EgVGhpw6puIEh14bq_LCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1571799161019!5m2!1svi!2s" width="100%" height="530" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
</div>