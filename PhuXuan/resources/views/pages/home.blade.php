@extends('layouts.default')
@push('head')
<meta name="keywords" content="hostel, nhà nghỉ, nhà nghỉ bình dân, bình dân, hue">
<meta name="author" content="stayhuehotel">
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta property="og:locale" content="vi_VN">
<meta property="og:type" content="article">
<meta property="og:title" content="{{ $information->name }}">
<meta property="og:url" content="">
<link rel="canonical" href="">
<meta property="og:image" content="{{ Voyager::image( method_exists($information, 'thumbnail') ? $information->thumbnail('cropped') : $information->image ) }}">
<meta property="og:image:alt" content="{{ $information->name }}">
<meta property="og:image:width" content="819">
<meta property="og:image:height" content="1024">
<meta property="og:description" content="Phú Xuân Hostel - điểm nghỉ dưỡng phù hợp cho bạn. Nằm ở vị trí gần trung tâm thành phố, từ nhà nghỉ Phú Xuân bạn có thể dễ dàng di chuyển đến các trung tâm giải trí lớn như VinCom, Big C hay các địa danh du lịch như Đại Nội, chùa Thiên Mụ... ">
@endpush
@section('title')
{{$information->name}}
@endsection
@section('content')
    <main>         
        @include('sections.home.hero_single')
        @include('sections.home.about')
        @include('sections.home.container-fluid')
        @include('sections.home.news_event')
        @include('sections.home.media-gallery')  
    </main>
@endsection
