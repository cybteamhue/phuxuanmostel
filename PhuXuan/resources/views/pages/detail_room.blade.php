@extends('layouts.layoutsroom')
@section('title')
{{$information->name}}-Chi tiết loại phòng
@endsection
@push('head')
<meta name="keywords" content="{{ $styleRoom->name }}">
<meta property="og:title" content="{{ $styleRoom->name }}">
<meta property="og:url" content="{{ asset('') }}{{ $styleRoom->slug }}">
<link rel="canonical" href="{{ asset('') }}{{ $styleRoom->slug }}">
<meta property="og:image" content="{{ Voyager::image( method_exists($styleRoom, 'thumbnail') ? $styleRoom->thumbnail('cropped') : $styleRoom->image)}}">
<meta property="og:image:alt" content="{{ $styleRoom->name }}">
<meta property="og:image:width" content="819">
<meta property="og:image:height" content="1024">
<meta property="og:description" content="{!! $styleRoom->contents !!}">
@endpush
@section('content')
    <main>             
        @include('sections.booking_room.results')
        @include('sections.booking_room.filters_listing')   
        <div class="container margin_60_35">               
            <div class="row">   
                    @include('sections.detail_room.book_now') 
                    <div class="col-lg-8">
                            <section id="description">
                                @include('sections.detail_room.sections_detail_room')
                                @include('sections.detail_room.other_room')
                            </section>
                    </div>
            </div>
        </div>    
    </main>
@endsection