
<head>
    <base href="{{ asset('') }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
    <title> @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons-->
    <link href="{{ Voyager::image( method_exists($information, 'thumbnail') ? $information->thumbnail('cropped') : $information->logo ) }}" rel="apple-touch-icon">
    <link href="{{ Voyager::image( method_exists($information, 'thumbnail') ? $information->thumbnail('cropped') : $information->logo ) }}" rel="icon">
    <link rel="shortcut icon" href="hotel/img/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="hotel/img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="hotel/img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="hotel/img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="hotel/img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="hotel/css/bootstrap.min.css" rel="stylesheet">
    <link href="hotel/css/style.css" rel="stylesheet">
	<link href="hotel/css/vendors.css" rel="stylesheet">
	<link href="hotel/css/phone.css" rel="stylesheet">
	<link href="hotel/css/chat.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="hotel/css/custom.css" rel="stylesheet">
    <link href="hotel/css/blog.css" rel="stylesheet">
    <!-- YOUR CUSTOM CSS -->
    <link href="hotel/css/custom.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    @stack('head')
</head>