<footer class="plus_border" id="lienhe">
    <div class="container margin_60_35">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h3 data-target="#collapse_ft_1">Các loại phòng</h3>
                <div class="collapse dont-collapse-sm" id="collapse_ft_1">
                    <ul class="links">
                        @foreach($styleRooms as $styleRoom)
                        <li><a href="{{ route('detail.Room',$styleRoom->slug) }}">{{$styleRoom->name}}</a></li>
                        @endforeach                        
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h3 data-target="#collapse_ft_2">Dịch vụ</h3>
                <div class="collapse dont-collapse-sm" id="collapse_ft_2">
                    <ul class="links">
                        @foreach($servicess as $service_other)
                            <li><a href="#0">{{$service_other->name}}</a></li>
                        @endforeach                     
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6" >
                <h3 data-target="#collapse_ft_3">Liên hệ</h3>
                <div class="collapse dont-collapse-sm" id="collapse_ft_3">
                    <ul class="contacts">
                        <li><i class="ti-home"></i>{{$information->name}}</li>
                        <li><i class="ti-headphone-alt"></i>{{$information->phone}}</li>
                        <li><i class="ti-email"></i><a href="#0">{{$information->email}}</a></li>
                        <li><i class=" icon-location-outline"></i><a href="#0">{{$information->address}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <h3 data-target="#collapse_ft_4">Keep in touch</h3>
                <div class="collapse dont-collapse-sm" id="collapse_ft_4">
                    <div id="newsletter">
                        <div id="message-newsletter"></div>
                        <form id="subscriber-form" action="javascript:void(0)" method="POST">
                            @csrf
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control" placeholder="Your email">
                                <input type="submit" value="Submit" id="send_form">
                            </div>
                        </form>
                    </div>
                    <div class="follow_us">
                        <h5>Follow Us</h5>
                        <ul>
                            <li><a href="https://www.{{$information->facebook}}"><i class="ti-facebook"></i></a></li>                      
                        </ul>
                    </div>                    
                </div>
            </div>
        </div>       
    </div>
    @include('shared.action.config-test')
    @include('shared.action.phone')


</footer>
<div id="toTop"></div>