<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Xác nhận đặt phòng</title>

</head>

<body>		
	
		<section  style="background-color: #EEEEEE; ">			
				<h4>XÁC NHẬN ĐẶT PHÒNG</h4>	
				<p>Xin chào Quý khách!</p>
				<p>Cảm ơn Quý khách đã đặt phòng tại tại khách sạn {{$name}}.</p>
				<p>Quý khách vui lòng kiểm tra và xác nhận thông tin đặt phòng ở bên dưới</p>
				<ul>
					<li style="margin-left: 15px;">Họ và tên (Contact person): {{ $nameuser }}</li>
					<li style="margin-left: 15px;">Điện thoại (Tell): {{ $phoneuser }}</li>						
					<li style="margin-left: 15px;">Ngày đến  (Arrival Date): {{ $date_of_arrival }}</li>
					<li style="margin-left: 15px;">Ngày đi  (Departure):{{ $date_of_department }}</li>				
					<li style="margin-left: 15px;">Loại phòng  (Room type): {{ $style_room }}</li>
					<li style="margin-left: 15px;">Số lượng phòng (No.of room):{{ $quality }}</li>						
				</ul>
					<strong>Đây là email tự động vui lòng không phản hồi lại, vì phản hồi lại chúng tôi sẽ không tiếp nhận được thông tin.</strong>
			<div  style="background-color: #FFCC00;">
				<div style="margin-left: 2%">
					<h4>Nếu quý khách có thắc mắc, xin vui lòng liên hệ với chúng tôi qua số điện thoại:{{$phone}} hoặc email: {{$emailhotel}}</h4>
					<p>Địa chỉ: {{$address}}</p>
				</div>
			</div>
		</section>

</body>
</html>
