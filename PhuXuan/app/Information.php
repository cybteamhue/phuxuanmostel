<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
class Information extends Model
{
    use Sluggable;
    protected $fillable = ['name','email','phone','address','facebook','content','contents','image','slug'];
    public function banner(){
        return $this->hasMany(Banner::class,'id_information','id');
      }
      public function sluggable()
      {
          return [
              'slug' => [
                  'source' => 'name'
              ]
          ];
      }
}
