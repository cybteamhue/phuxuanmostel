<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class StyleRoom extends Model
{
    use Sluggable;
    public function imageRoom(){
        return $this->hasMany(imageRoom::class,'id_style_room','id');
      }
      public function sluggable()
      {
          return [
              'slug' => [
                  'source' => 'name'
              ]
          ];
      }
}
