<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingRoom extends Model
{
    protected $fillable = ['name','email','phone','style_room','date_of_arrival','date_of_department','quality'];
}
