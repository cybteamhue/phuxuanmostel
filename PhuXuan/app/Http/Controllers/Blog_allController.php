<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Information;
use App\StyleRoom;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\Post;
use App\ServiceOther;

class Blog_allController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    public function __invoke(Request $request,Information $informationModel, ServiceOther $serviceOtherModel,StyleRoom $styleRoomModel, Category $categoryModel,Post $postModel)
    {
        $information = $informationModel->get()->first();
        $styleRooms = $styleRoomModel->get();
        $categories = $categoryModel->get();
        $posts = $postModel->paginate(8);
        $servicess =$serviceOtherModel->get();
        $viewData = compact('information','styleRooms','categories','posts','servicess');
        return view('pages.blog_all',$viewData);    }

    public function blog($slug, ServiceOther $serviceOtherModel)
    {
        $post = Post::where('slug',$slug)->firstOrFail();
        $styleRooms = StyleRoom::get();
        $information = Information::get()->first();
        $categories = Category::get();
        $servicess =$serviceOtherModel->get();
        $viewData = compact('post','information','styleRooms','categories','servicess');
        return view('pages.detail_blog',$viewData);
    }
    public function getSearch(Request $request, ServiceOther $serviceOtherModel)
    {
        $information = Information::get()->first();
        $styleRooms = StyleRoom::get();
        $categories = Category::get();
        $servicess =$serviceOtherModel->get();
        $SearchTours = Post::where('title','like','%'.$request->keyword.'%')
                        ->paginate(8);;
        return view('pages.search_blog',compact('SearchTours','information','styleRooms','categories','servicess'));
    }
    
    }
