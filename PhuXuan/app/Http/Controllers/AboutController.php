<?php

namespace App\Http\Controllers;

use App\Information;
use App\StyleRoom;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Category;

class AboutController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, StyleRoom $styleRoomModel,Information $informationModel, Category $categoryModel)
    {
        $information = $informationModel->get()->first();
        $styleRooms = $styleRoomModel->get();
        $categories = $categoryModel->get();
        $viewData = compact('styleRooms','information','categories');
        return view('pages.about',$viewData);
    }
}
