<?php

namespace App\Http\Controllers;

use App\Information;
use App\StyleRoom;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Category;
use TCG\Voyager\Models\Post;
use App\ServiceOther;

class BlogController extends Controller
{

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($slug,Request $request,ServiceOther $serviceOtherModel,Information $informationModel,StyleRoom $styleRoomModel, Category $categoryModel,Post $postModel)
    {
        $information = $informationModel->get()->first();
        $styleRooms = $styleRoomModel->get();
        $categories = $categoryModel->get();
        $categoriess = Category::where('slug',$slug)->firstOrFail();
        $posts = $postModel->paginate(8);
        $servicess =$serviceOtherModel->get();
        $viewData = compact('information','styleRooms','categories','posts','servicess','categoriess');
        return view('pages.blog',$viewData);
    }
    
    
}
