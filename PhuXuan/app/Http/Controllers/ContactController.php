<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Information;
use App\StyleRoom;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Category;

class ContactController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request,Information $informationModel,StyleRoom $styleRoomModel, Category $categoryModel)
    {
        $information = $informationModel->get()->first();
        $styleRooms =$styleRoomModel->get();
        $categories = $categoryModel->get();
        $viewData = compact('information','styleRooms','categories');
        return view('pages.contact',$viewData);
    }
    public function feedback(Request $request)
    {
        $feedback = Feedback::create($request->all());
        return redirect()->back();
    }
}
